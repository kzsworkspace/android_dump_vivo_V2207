## full_k69v1_64_k419-user 12 SP1A.210812.003 compiler12171243 release-keys
- Manufacturer: vivo
- Platform: mt6768
- Codename: V2207
- Brand: vivo
- Flavor: full_k69v1_64_k419-user
- Release Version: 12
- Kernel Version: 4.19.191
- Id: SP1A.210812.003
- Incremental: compiler12171243
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Treble Device: true
- Locale: en-US
- Screen Density: 300
- Fingerprint: vivo/V2207/V2207:12/SP1A.210812.003/compiler12171243:user/release-keys
- OTA version: 
- Branch: full_k69v1_64_k419-user-12-SP1A.210812.003-compiler12171243-release-keys
- Repo: vivo_v2207_dump
