#!/system/bin/sh

/system/bin/dmesg -S | system/bin/grep -e TeeSeApi -e OMATA -e SE_TA -e TeeSeApi -e Nxp -e SN100 > /data/SecureElement/logData/log/kernellog
chmod 777 /data/SecureElement/logData/log/kernellog
/system/bin/logcat -d | system/bin/grep -e SecureElement -e eSE-HAL -e VSES -e NxpNci -e data_trace > /data/SecureElement/logData/log/mainlog
chmod 777 /data/SecureElement/logData/log/mainlog
setprop persist.vendor.vivo.catselog finish